<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <p>
            <?php
            $utilisateur1 = [
                "nom" => "Slime",
                "prenom" => "Miracle",
                "login" => "slimem"
            ];
            $utilisateur2 = [
                    "nom" => "GODEK",
                "prenom" => "Paul",
                "login" => "godekp"
            ];
            $utilisateur3 = [
                    "nom" => "MON PREMIER",
                "prenom" => "MON DEUXIEME",
                "login" => "monpremierm"
            ];
            $utilisateurs = [];
            $utilisateurs[] = $utilisateur1;
            $utilisateurs[] = $utilisateur2;
            $utilisateurs[] = $utilisateur3;

            echo "<ul>";
            if (!empty($utilisateurs)) {
                foreach ($utilisateurs as $utilisateur) {
                    echo "<li>Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]</li>";
                }
            } else {
                echo "<li>Il n'y a aucun utilisateur</li>";
            }
            echo "</ul>";
            ?>
        </p>
    </body>
</html> 