<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Test de ma classe Utilisateur.php </title>
</head>

<body>
<p>
    <?php
    require_once 'Trajet.php';
    $departTrajet = $_GET["depart"];
    $arriveeTrajet = $_GET["arrivee"];
    $dateTrajet = new DateTime($_GET["date"]);
    $prixTrajet = $_GET["prix"];
    $conducteurLoginTrajet = Utilisateur::recupererUtilisateurParLogin($_GET["conducteurLogin"]);
    $nonFumeurTrajet = isset($_GET["nonFumeur"]);
    $trajet = new Trajet(null, $departTrajet, $arriveeTrajet, $dateTrajet, $prixTrajet, $conducteurLoginTrajet, $nonFumeurTrajet);
    $trajet->ajouter();
    ?>
</p>
</body>
</html>