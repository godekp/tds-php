<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Lecture des utilisateurs de la Base de Données </title>
</head>

<body>
<p>
    <?php
    include 'ConnexionBaseDeDonnees.php';
    require_once 'Utilisateur.php';
    foreach (Utilisateur::getUtilisateurs() as $utilisateur) {
        echo $utilisateur;
        if (($trajetEnTantQuePasser = $utilisateur->getTrajetsCommePassager()) != null) {
            echo "Cette personne participe aux trajets suivants : ";
        }
        foreach ($trajetEnTantQuePasser as $trajetCommePassager) {
            echo "<p>- Départ : " . $trajetCommePassager->getDepart() . ", Arrivée : " . $trajetCommePassager->getArrivee() . "</p>";
        }
        echo "<br>";
    }
    ?>
</p>
</body>
</html>

