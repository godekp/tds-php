<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Lecture des trajets de la Base de Données </title>
</head>

<body>
<p>
    <?php
    include 'ConnexionBaseDeDonnees.php';
    require_once 'Trajet.php';
    foreach (Trajet::recupererTrajets() as $trajet) {
        echo $trajet;
        if (($passagersDuTrajet = $trajet->getPassagers()) != null) {
            echo "Ses passagers sont : ";
        }
        foreach ($passagersDuTrajet as $passager) {
            echo "<p>- {$passager->getPrenom()} {$passager->getNom()} (<a href='supprimerPassager.php?login={$passager->getLogin()}&trajet_id={$trajet->getId()}'>désinscrire ?</a>)</p>";
        }
        echo "<br>";
    }
    ?>
</p>
</body>
</html>

