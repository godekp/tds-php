<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        if ($dureeExpiration === null) {
            $dureeExpiration = 0;
        }
        setcookie($cle, serialize($valeur), $dureeExpiration);
    }

    public static function lire(string $cle) : mixed {
        if (self::contient($cle)) {
            return unserialize($_COOKIE[$cle]);
        }
        return null;
    }

    public static function contient($cle) : bool {
        if (isset($_COOKIE[$cle])) {
            return true;
        }
        return false;
    }

    public static function supprimer($cle) : void {
        unset($_COOKIE[$cle]);
        setcookie ($cle, "", 1);
    }
}