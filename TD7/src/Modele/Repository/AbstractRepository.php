<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $listeColonnes = array();
        foreach ($this->getNomsColonnes() as $colonne) {
            $listeColonnes[] = $colonne . " = :" . $colonne . "Tag";
        }
        $arguments = join(', ', $listeColonnes);
        $clePrimaire = $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire() . "Tag";

        $sql = "UPDATE {$this->getNomTable()} SET {$arguments} WHERE {$clePrimaire};";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $arguments = $this->getNomClePrimaire().", ".join(', ', $this->getNomsColonnes());
        $listeColonnes = array();
        foreach ($this->getNomsColonnes() as $colonne) {
            $listeColonnes[] = ":" . $colonne . "Tag";
        }
        $tags = join(', ', $listeColonnes);
        $tags = ":".$this->getNomClePrimaire()."Tag, ".$tags;

        $sql = "INSERT INTO {$this->getNomTable()} ({$arguments}) VALUES ($tags);";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = $this->formatTableauSQL($objet);

        return $pdoStatement->execute($values);
    }

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    /**
     * @return string[]
     */
    protected abstract function getNomsColonnes() : array;

    public function supprimer(string $valeurClePrimaire): bool
    {
        $sql = "DELETE FROM {$this->getNomTable()} WHERE {$this->getNomClePrimaire()} = :clePrimaireTag;";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $valeurClePrimaire
        );

        return $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from {$this->getNomTable()} WHERE {$this->getNomClePrimaire()} = :clePrimaireTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $clePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();
        if (!$objetFormatTableau)
            return null;

        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    protected abstract function getNomTable() : string;
    protected abstract function getNomClePrimaire() : string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    public function recuperer() : array
    {
        $table = $this->getNomTable();
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM $table");

        $tableau = [];
        foreach ($pdoStatement as $objFormatTableau) {
            $tableau[] = $this->construireDepuisTableauSQL($objFormatTableau);
        }
        return $tableau;
    }


}