<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use DateTime;
use PDO;

class TrajetRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $objetFormatTableau) : Trajet {
        $trajet = new Trajet(
            $objetFormatTableau["id"],
            $objetFormatTableau["depart"],
            $objetFormatTableau["arrivee"],
            new DateTime($objetFormatTableau["date"]),
            $objetFormatTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($objetFormatTableau["conducteurLogin"]),
            $objetFormatTableau["nonFumeur"]
        );
        $trajet -> setPassagers(TrajetRepository::recupererPassagers($trajet));

        return $trajet;
    }

    /**
     * @return Trajet[]
     */
    /*public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = self::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    /**
     * @return Utilisateur[]
     */
    public static function recupererPassagers(Trajet $trajet) : array {
        $sql = 'SELECT u.login, u.nom, u.prenom
                FROM passager p
                JOIN utilisateur u ON p.passagerLogin = u.login
                WHERE p.trajetId = :trajetIdTag;';

        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);

        $values = array(
            'trajetIdTag' =>$trajet->getId()
        );

        $pdoStatement -> execute($values);

        $listeUtilisateur = $pdoStatement -> fetchAll(PDO::FETCH_ASSOC);

        $Utilisateur = array();
        foreach($listeUtilisateur as $utilisateurFormatted) {
            $Utilisateur[] = (new UtilisateurRepository())->construireDepuisTableauSQL($utilisateurFormatted);
        }

        return $Utilisateur;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        $isFumeur = 1;
        if ($trajet->isNonFumeur()) {
            $isFumeur = 0;
        }
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => "".$trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $isFumeur
        );
    }
}