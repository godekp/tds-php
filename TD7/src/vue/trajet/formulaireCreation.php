<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="creerDepuisFormulaire"/>
    <input type="hidden" name="controleur" value="trajet"/>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="id_id">Id&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="null" name="id" id="id_id" readonly>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Départ&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Montpellier" name="depart" id="depart_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Nîmes" name="arrivee" id="arrivee_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="Ex : 30/04/2005" name="date" id="date_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="Ex : 5" name="prix" id="prix_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Conducteur&#42;</label>
            <select class="InputAddOn-field" name="conducteurLogin" id="conducteurLogin_id"
                   required>
                <?php
                /** @var Utilisateur[] $utilisateurs */

                use App\Covoiturage\Modele\DataObject\Utilisateur;

                foreach($utilisateurs as $utilisateur){
                    echo "<option value='".htmlspecialchars($utilisateur->getLogin())."'>".htmlspecialchars($utilisateur->getPrenom())." ".htmlspecialchars($utilisateur->getNom())."</option>";
                }
                ?>
            </select>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non fumeur ?&#42;</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="Ex : Nîmes" name="nonFumeur" id="nonFumeur_id">
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>