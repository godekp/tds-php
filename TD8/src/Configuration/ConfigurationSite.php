<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(
        "dureeExpiration" => 300
    );

    public static function getDureeExpiration(): int {
        return self::$configurationSite["dureeExpiration"];
    }
}