<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur de Contrôleur", "cheminCorpsVue" => "erreur.php"]);
    }

    public static function afficherFormulairePreference () : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de Préférence", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference() : void {
        if (isset($_GET["controleur_defaut"])) {
            PreferenceControleur::enregistrer($_GET["controleur_defaut"]);
            self::afficherVue('vueGenerale.php', ["titre" => "Préférences Enregistrées", "cheminCorpsVue" => "preferenceEnregistree.php"]);
        } else {
            self::afficherErreur('La valeur $_GET["controleur_default"] n\'existe pas');
        }
    }
}