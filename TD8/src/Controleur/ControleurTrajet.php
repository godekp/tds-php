<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;
use TypeError;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherFormulaireMiseAJour() : void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "trajet" => $trajet, "titre" => "Formulaire de mise à jour de trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
    }

    public static function mettreAJour() : void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "id" => $trajet->getId(), "titre" => "Mise à jour du trajet d'id {$trajet->getId()}", "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);
    }

    public static function supprimer() : void
    {
        $id = $_GET["id"];
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "trajetId" => $id, "titre" => "Suppression de trajet", "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
    }

    public static function afficherFormulaireCreation(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Formulaire de création de trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["trajets" => $trajets, "titre" => "Création de trajet", "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    public static function afficherDetail(): void
    {
        try {
            $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
            if ($trajet == NULL) {
                self::afficherErreur("Le trajet d'id {$_GET['id']} n'existe pas");
            } else {
                self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Détail du trajet de {$trajet->getDepart()} à {$trajet->getArrivee()}", "cheminCorpsVue" => "trajet/detail.php"]);
            }
        } catch (TypeError $e) {
            self::afficherErreur("Jsp ce qu'il s'est passé dsl, voilà l'erreur : {$e->getMessage()}");
        }
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur avec Trajet", "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    private static function isFumeur(array $tableauDonnees): bool
    {
        $fumeur = true;
        if (isset($tableauDonnees['nonFumeur'])) {
            $fumeur = false;
        }
        return $fumeur;
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $fumeur = self::isFumeur($tableauDonneesFormulaire);
        return new Trajet((int)$tableauDonneesFormulaire["id"]??null, $tableauDonneesFormulaire["depart"], $tableauDonneesFormulaire["arrivee"], new DateTime($tableauDonneesFormulaire["date"]), (int)$tableauDonneesFormulaire["prix"], (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire["conducteurLogin"]), $fumeur);
    }
}