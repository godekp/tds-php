<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use TypeError;

class ControleurUtilisateur extends ControleurGenerique
{
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        try {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if ($utilisateur == NULL) {
                self::afficherErreur("L'utilisateur de login {$_GET['login']} n'existe pas");
            } else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Détail de {$utilisateur->getPrenom()} {$utilisateur->getNom()}", "cheminCorpsVue" => "utilisateur/detail.php"]);
            }
        } catch (TypeError $e) {
            self::afficherErreur("Jsp ce qu'il s'est passé dsl, voilà l'erreur : {$e->getMessage()}");
        }
    }
/*
    public static function deposerCookie() {
        Cookie::enregistrer("test", [6, 7, 5, 4, 1, 456789]);
    }

    public static function lireCookie() {
        echo Cookie::lire("test");
    }
*/
    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de création d'utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        if (strcmp($_GET["mdp"], $_GET["mdp2"]) === 0) {
                $user = self::construireDepuisFormulaire($_GET);
                if (!ConnexionUtilisateur::estAdministrateur()) {
                    $user->setEstAdmin(false);
                }
                (new UtilisateurRepository())->ajouter($user);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                if (!ConnexionUtilisateur::estConnecte()) {
                    ConnexionUtilisateur::connecter($user->getLogin());
                }
                self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Création d'utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        } else {
            self::afficherErreur("Mots de passe distincts");
        }
    }

    public static function afficherErreur(string $messageErreur = "") : void
    {
        self::afficherVue('vueGenerale.php', ["messageErreur" => $messageErreur, "titre" => "Erreur avec Utilisateur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimer() : void
    {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            $user = (new UtilisateurRepository())->recupererParClePrimaire($_GET["login"]);
            if (!is_null($user)) {
                if (ConnexionUtilisateur::estUtilisateur($login)) {
                    (new UtilisateurRepository())->supprimer($login);
                    $utilisateurs = (new UtilisateurRepository())->recuperer();
                    if (strcmp(ConnexionUtilisateur::getLoginUtilisateurConnecte(), $login) === 0) {
                        ConnexionUtilisateur::deconnecter();
                    }
                    self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $login, "titre" => "Suppression d'utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
                } else {
                    self::afficherErreur("Il faut être connecté pour mettre à jour un compte");
                }
            } else {
                self::afficherErreur("Le login renseigné n'existe pas");
            }
        } else {
            self::afficherErreur("Tous les champs du formulaire n'ont pas été renseigné");
        }
    }

    public static function afficherFormulaireMiseAJour() : void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (!is_null($utilisateur)) {
            if (ConnexionUtilisateur::estAdministrateur() || ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Formulaire de mise à jour d'utilisateur", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
            } else {
                self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            }
        } else {
            self::afficherErreur("Login inconnu");
        }
    }

    private static function effectuerMiseAJour(AbstractDataObject $user) : void {
        $user->setNom($_GET["nom"]);
        $user->setPrenom($_GET["prenom"]);
        $user->setMdpHache(MotDePasse::hacher($_GET["mdp"]));
        if (ConnexionUtilisateur::estAdministrateur()) {
            $user->setEstAdmin(self::isEstAdmin($_GET));
        }
        (new UtilisateurRepository())->mettreAJour($user);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs, "login" => $user->getLogin(), "titre" => "Mise à jour de l'utilisateur de login {$user->getLogin()}", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
    }

    public static function mettreAJour() : void
    {
        if (isset($_GET["login"]) && isset($_GET["nom"]) && isset($_GET["prenom"]) && isset($_GET["old_mdp"]) && isset($_GET["mdp"]) && isset($_GET["mdp2"])) {
            $user = (new UtilisateurRepository())->recupererParClePrimaire($_GET["login"]);
            if (!is_null($user)) {
                if (strcmp($_GET["mdp"], $_GET["mdp2"]) === 0) {
                    if (ConnexionUtilisateur::estAdministrateur() || MotDePasse::verifier($_GET["old_mdp"], $user->getMdpHache())) {
                        if (ConnexionUtilisateur::estAdministrateur() || ConnexionUtilisateur::estUtilisateur($user->getLogin())) {
                            self::effectuerMiseAJour($user);
                        } else {
                            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
                        }
                    } else {
                        self::afficherErreur("L'ancien mot de passe est incorrect");
                    }
                } else {
                    self::afficherErreur("Les mots de passe distincts");
                }
            } else {
                self::afficherErreur("Login inconnu");
            }
        } else {
            self::afficherErreur("Tous les champs du formulaire n'ont pas été renseigné");
        }
    }

    private static function isEstAdmin(array $tableauDonnees): bool
    {
        $estAdmin = false;
        if (isset($tableauDonnees['estAdmin'])) {
            $estAdmin = true;
        }
        return $estAdmin;
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"], MotDePasse::hacher($tableauDonneesFormulaire["mdp"]), self::isEstAdmin($tableauDonneesFormulaire));
    }

    public static function afficherFormulaireConnexion() : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Formulaire de connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter() : void {
        if (isset($_GET["login"]) && isset($_GET["mdp"])) {
            $user = (new UtilisateurRepository())->recupererParClePrimaire($_GET["login"]);
            if (!is_null($user) && MotDePasse::verifier($_GET["mdp"], $user->getMdpHache())) {
                ConnexionUtilisateur::connecter($_GET["login"]);
                self::afficherVue('vueGenerale.php', ["utilisateur" => $user, "titre" => "Utilisateur connecté", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"]);
            } else {
                self::afficherErreur("Login et/ou mot de passe incorrect");
            }
        } else {
            self::afficherErreur("Login et/ou mot de passe manquant");
        }
    }

    public static function deconnecter() : void {
        ConnexionUtilisateur::deconnecter();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => (new UtilisateurRepository())->recuperer(), "titre" => "Utilisateur déconnecté", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);
    }

    public static function demarrerSession() {
        Session::getInstance();
    }

    public static function ecrireSession() {
        Session::getInstance()->enregistrer("login", "godekp");
    }

    public static function lireSession() {
        echo Session::getInstance()->lire("login");
    }

    public static function supprimerSession() {
        Session::getInstance()->supprimer("login");
    }

    public static function detruireSession() {
        Session::getInstance()->detruire();
    }

}