<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Cookie;

class PreferenceControleur {
    private static string $clePreference = "preferenceControleur";

    public static function enregistrer(string $preference) : void
    {
        Cookie::enregistrer(PreferenceControleur::$clePreference, $preference);
    }

    public static function lire() : string
    {
        if (self::existe())
            return Cookie::lire(self::$clePreference);
        return "";
    }

    public static function existe() : bool
    {
        if (Cookie::contient(self::$clePreference)) {
            return true;
        }
        return false;
    }

    public static function supprimer() : void
    {
        Cookie::supprimer(self::$clePreference);
    }
}

