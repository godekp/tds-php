<?php
namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
class Utilisateur extends AbstractDataObject
{

    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;
    private string $mdpHache;
    private bool $estAdmin;

    public function __construct(string $login, string $nom, string $prenom, string $mdpHache, bool $estAdmin = false)
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
    }

    public function getEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if (is_null($this->trajetsCommePassager))
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public function getNom() : string
    {
        return $this->nom;
    }

    public function setNom(string $nom) : void
    {
        $this->nom = $nom;
    }

    public function getLogin() : string
    {
        return $this->login;
    }
    public function setLogin(string $login) : void
    {
        $this->login = substr($login, 0, 64);
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString() : string
    {
        return "<p>Utilisateur $this->prenom $this->nom de login $this->login.</p>";
    }*/
}