<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDO;

class UtilisateurRepository extends AbstractRepository
{

    protected function construireDepuisTableauSQL(array $objetFormatTableau) : Utilisateur
    {
        return new Utilisateur($objetFormatTableau['login'],
            $objetFormatTableau['nom'],
            $objetFormatTableau['prenom'],
            $objetFormatTableau["mdpHache"],
            $objetFormatTableau["estAdmin"],);
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $user) : array {
        $sql = 'SELECT p.trajetId
                FROM passager p
                WHERE p.passagerLogin = :loginTag;';

        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);

        $values = array(
            'loginTag' => $user->getLogin()
        );

        $pdoStatement -> execute($values);

        $listeTrajet = $pdoStatement -> fetchAll(PDO::FETCH_ASSOC);

        $Trajets = array();
        foreach($listeTrajet as $trajetFormatted) {
            $Trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatted);
        }

        return $Trajets;
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["nom", "prenom", "mdpHache", "estAdmin"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        $admin = 0;
        if ($utilisateur->getEstAdmin()) {
            $admin = 1;
        }
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $admin
        );
    }
}