<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="enregistrerPreference">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php use App\Covoiturage\Lib\PreferenceControleur;
            if (strcmp(PreferenceControleur::lire(), "utilisateur") === 0) echo "checked"?>>
            <label class="InputAddOn-item" for="utilisateurId">Utilisateur</label>
        </p>
        <p class="InputAddOn">
            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php
            if (strcmp(PreferenceControleur::lire(), "trajet") === 0) echo "checked"?>>
            <label class="InputAddOn-item" for="trajetId">Trajet</label>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit">
        </p>
    </fieldset>

</form>