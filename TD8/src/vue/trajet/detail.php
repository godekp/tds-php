<?php
/** @var Trajet $trajet */

use App\Covoiturage\Modele\DataObject\Trajet;

$fumeurOuNon = "fumeur";
if ($trajet->isNonFumeur()) {
    $fumeurOuNon = "non fumeur";
}
echo "Le trajet " . $fumeurOuNon . " d'id " . htmlspecialchars($trajet->getId()) . ", de départ " . htmlspecialchars($trajet->getDepart()) . " et d'arrivée " . htmlspecialchars($trajet->getArrivee()) . " coûtera " . htmlspecialchars($trajet->getPrix()) . "€ et partira le " . htmlspecialchars($trajet->getDate()->format("d/m/y")) . " à " . htmlspecialchars($trajet->getDate()->format("H:i")) . " avec pour conducteur " . htmlspecialchars($trajet->getConducteur()->getPrenom()) . " " . htmlspecialchars($trajet->getConducteur()->getNom()) . ".";