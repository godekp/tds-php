<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

echo "<h2>Liste des trajets</h2><ul>";
foreach ($trajets as $trajet) {
    $idHTML = htmlspecialchars($trajet->getId());
    $idURL = rawurlencode($trajet->getId());
    echo '<li><p> Trajet d\'ID <a href="controleurFrontal.php?action=afficherDetail&controleur=trajet&id=' . $idURL . '">' . $idHTML . '</a> (<a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=trajet&id=' . $idURL . '">Modifier ?</a>, <a href="controleurFrontal.php?action=supprimer&controleur=trajet&id=' . $idURL . '">Supprimer ?</a>)</p></li>';
}
echo '</ul><p><a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet">Créer un trajet</a></p>';