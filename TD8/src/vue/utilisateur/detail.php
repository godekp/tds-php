<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

$loginURL = rawurlencode($utilisateur->getLogin());

$deBase = '<p>Le login de l\'utilisateur ' . htmlspecialchars($utilisateur->getPrenom()) . ' ' . htmlspecialchars($utilisateur->getNom()) . ' est ' . htmlspecialchars($utilisateur->getLogin()) . '. </p>';
$petitPlus = ' (<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier ?</a>, <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer ?</a>)';

if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
    $deBase = $deBase . $petitPlus;
}
echo $deBase;