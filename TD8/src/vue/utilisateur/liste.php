<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

echo "<h2>Liste des utilisateurs</h2><ul>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<li><p> Utilisateur de login <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">' . $loginHTML;
    if (ConnexionUtilisateur::estAdministrateur()) {
        echo ' (<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier ?</a>, <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer ?</a>)';
    }
    echo '</a></p></li>';
}
echo '</ul><p><a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a></p>';