<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title><?php /** @var string $titre */

            use App\Covoiturage\Lib\ConnexionUtilisateur;

            echo $titre; ?></title>
        <link rel="stylesheet" type="text/css" href="../ressources/css/navstyle.css">

    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference">
                            <img src="../ressources/img/heart.png"  alt="coeur">
                        </a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur">
                            <img src="../ressources/img/add-user.png" alt="ajouter-utilisateur">
                        </a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li>
                    <?php
                    if (!ConnexionUtilisateur::estConnecte()) {
                        echo '<li>
                            <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur">
                                <img src="../ressources/img/enter.png" alt="se-connecter">
                            </a>
                        </li>';
                    } else {
                        echo '<li>
                            <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte()) . '">
                                <img src="../ressources/img/user.png" alt="ajouter-utilisateur">
                            </a>
                        </li>
                        <li>
                            <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur">
                                <img src="../ressources/img/logout.png" alt="se-connecter">
                            </a>
                        </li>';
                    }
                    ?>
                </ul>
            </nav>


        </header>
        <main>
            <?php
            /** @var string $cheminCorpsVue */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Super site de covoiturage de Paul Godek, le goat originel
            </p>
        </footer>
    </body>
</html>