<?php

use App\Covoiturage\Controleur\ControleurGenerique;
use App\Covoiturage\Lib\PreferenceControleur;

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// On récupère l'action passée dans l'URL
$controleur = PreferenceControleur::lire();
if (strcmp($controleur, "") === 0) {
    $controleur = "utilisateur";
}
if (isset($_GET["controleur"])) {
    $controleur = $_GET["controleur"];
}
$nomDeClasseControlleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);

if (class_exists($nomDeClasseControlleur)) {
    if (isset($_GET["action"])) {
        $listeFonction = get_class_methods($nomDeClasseControlleur); // Ou simplement ControleurUtilisateur::class
        if ($listeFonction != null && in_array($_GET["action"], $listeFonction)) {
            $action = $_GET["action"];
            // Appel de la méthode statique $action de ControleurUtilisateur
            $nomDeClasseControlleur::$action();
        } else {
            $nomDeClasseControlleur::afficherErreur("Cette action n'existe pas");
        }
    } else {
        $nomDeClasseControlleur::afficherListe();
    }
} else {
    ControleurGenerique::afficherErreur("Pas de contrôleur du nom " . $nomDeClasseControlleur);
}